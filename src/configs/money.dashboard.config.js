export const TableConfig = ({
  name: 'MoneyTable',

  perPage: 26,
  totals: true,
  disableSort: false,
  disablePagination: false,
  totalColumn: 'fullAddress',
  caption: {
    color: '#fff',
    borderColor: '#FFB400',
    text: 'דוח מצב גביה לבניינים',
    backgroundColor: 'rgba(55, 137, 200, 0.76)',
  },
  headers: [
    {
      value: 'fullAddress',
      text: 'בניין/ מתחם',
      width: 200
    },
    {
      value: 'workerName',
      text: 'אחראי גביה',
    },
    {
      text: 'לגביה עד סוף השנה (%)',
      value: 'collectByEOYearPercent',
    },
    {
      text: 'לגביה עד סוף החודש (%)',
      value: 'collectByEomPercent',
    },
    {
      text: 'תקשורות בחודש האחרון',
      value: 'commLast2Weeks',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'גביה אחר (%)',
      value: 'otherColPercent',
    },
    {
      text: 'חוב שנים קודמות',
      value: 'prevYearDebt'
    },
    {
      text: 'גביה מיוחדת (%)',
      value: 'specialColPercent',
    },
    {
      text: 'לגביה עד סוף החודש',
      value: 'toCollectByEOM',
    },
    {
      text: 'יחידות בטיפול משפטי',
      value: 'unitsInLaw',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
  ],
  itemsPerPage: 26,
  footerProps: {
    itemsPerPageAllText: 'את כל',
    itemsPerPageText: 'שורות לעמוד:',
    itemsPerPageOptions: [26, 52, -1],
  },
})

export const Table1Config = ({
  name: 'MoneyTable1',

  totals: true,
  disableSort: false,
  disablePagination: true,
  totalColumn: 'workerName',
  caption: {
    color: '#fff',
    text: 'דוח מצב אחראי גביה',
    borderColor: '#FFB400',
    backgroundColor: 'rgba(55, 137, 200, 0.76)',
  },
  headers: [
    {
      text: 'אחראי גביה',
      value: 'workerName',
    },
    {
      text: 'יחידות לגביה',
      value: 'unitCount',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'הוק חדשות היום',
      value: 'newHokCntToday',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'הוק אשראי חדשות החודש',
      value: 'newHokCntThisMonth',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'גביה היום',
      value: 'collectionToday',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'שיחות השבוע',
      value: 'phoneCntToday',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'מכתבים/ מיילים היום',
      value: 'emailOrLetterCntToday',
      highlighting: {
        min: {
          color: 'success',
        },
        max: {
          color: 'error',
        },
      },
    },
    {
      text: 'סמס היום',
      value: 'smsCntToday',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'גביה השבוע',
      value: 'collectionWeekStart',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'שיחות השבוע',
      value: 'phoneCntWeekStart',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'מיילים השבוע',
      value: 'emailOrLetterCntWeekStart',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'סמסים השבוע',
      value: 'smsCntWeekStart',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
  ],
})

export const Table2Config = ({
  caption: {
    color: '#fff',
    borderColor: '#F49AC1',
    text: 'מצב גביה בשבוע אחרון',
    backgroundColor: 'rgba(55, 137, 200, 0.76)',
  },
})