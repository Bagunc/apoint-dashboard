const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  "transpileDependencies": [
    "vuetify",
    "query-string",
    "split-on-first",
    "strict-uri-encode"
  ],
  configureWebpack: {
    devtool: 'source-map',
    optimization: {
      minimizer: [
        new UglifyJsPlugin()
      ],
    },
  },
  publicPath: '/dashboard/',
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        args[0].title = 'Build-App Dashboard';
        return args;
      });
  }
}