// UNIT_VER = "1.01";

// export default function UnitVersion() { return `Utils unit version is: ${UNIT_VER}`}

//VER 1.01

/**
 * Encrypt module with secret key
 */
var encryptor = require('simple-encryptor')('6FTaMvY4uGUek8rir2QnUEeXP9TUpkjK');

export function Nz(value, zeroValue) {
  if (zeroValue === undefined) zeroValue = 0

  try {
    if (value === null || value === undefined)
      return zeroValue
    else
      return value
  } catch {
    return zeroValue
  }
}
 
export function getDate(today) {
  let date

  if (today !== undefined)
    date = today
  else
    date = new Date()

  //he-IL returns date separated with '.' instead of '/' like en-GB
  const time = new Intl.DateTimeFormat('en-GB').format(date)
  return time
}

export function getDateTime(today, showSeconds = false) {
  let date
  //initial value
  if (today !== undefined)
    date = today
  else
    date = new Date()

  //https://devhints.io/wip/intl-datetime
  const options ={
    year: '2-digit',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  }

  if (showSeconds)
    options.second = 'numeric'
  //he-IL returns date separated with '.' instead of '/' like en-GB
  const time = new Intl.DateTimeFormat('en-GB',options).format(date)
  return time
}

/**
 * Checking is online mode
 * 
 * @param { Array<Date> } range
 * 
 * @returns boolean
 */
export function isOnline(range) {

  const [Start, End] = range

  const Now = new Date()

  return Start < Now && Now < End
}

/**
 * Getting start end online range with time(string) format
 * 
 * @param { string } s 
 * @param { string } e 
 * 
 * @returns Array<Date>
 */
export function getOnlineRanges(s, e) {

  const [endHour, endMinut, endSecond] = e.split(':')
  const [startHour, startMinut, startSecond] = s.split(':')
  
  const Start = new Date()
  if(startHour)
    Start.setHours(startHour)

  if(startMinut)
    Start.setMinutes(startMinut)

  if(startSecond)
    Start.setSeconds(startSecond)
  
  const End = new Date()
  if(endHour)
    End.setHours(endHour)

  if(endMinut)
    End.setMinutes(endMinut)

  if(endSecond)
    End.setSeconds(endSecond)

  if (Start > End)
    Start.setDate(Start.getDate() - 1)

  return [Start, End]
}

/**
 * Tables per page getter and setter(value !empty)
 * Options is saves in localStorage
 * 
 * @param { string } tableName 
 * @param { number } value 
 * 
 * @returns number | undefined
 */
export function tablePerPage(tableName, value) {

  let storage = localStorage.getItem('apointPerPage')

  try {
    storage = JSON.parse(storage) || {}
  } catch (error) {
    storage = {}
  }

  if (value) {
    storage[tableName] = value

    return localStorage.setItem('apointPerPage', JSON.stringify(storage))
  }

  return storage[tableName]
}

/**
 * Chekcing in localStorage is application Authenticated
 * 
 * @returns boolean
 */
export const hasAuth = () => localStorage.getItem('appd') || sessionStorage.getItem('appd') ? true : false;

/**
 * Logout from system
 * 
 * @returns undefined
 * 
 */
export const logout = () => localStorage.removeItem('appd') & sessionStorage.removeItem('appd')

/**
 * Getting encrytped data from localStorage
 * Read priority sessionStorage
 * 
 * @param { string } key
 * 
 * @returns any
 */
export function getPrivateData(key) {

  const value = sessionStorage.getItem(key) || localStorage.getItem(key)

  if (value)
    return encryptor.decrypt(value)

  return null
}

/**
 * Encrypting and saving data in localStorage/sessionStorage
 * 
 * @param { string } key
 * @param { any } value
 * @param { boolean } value
 * 
 * @returns undefined
 */
export function savePrivateData(key, value, save = false) {

  const encrypted = encryptor.encrypt(value);

  save && localStorage.setItem(key, encrypted)

  return  sessionStorage.setItem(key, encrypted)
}

//JSX component
// export function ShowDateTimeCurrent(props) {
//    const [today, setToday] = useState(Date.now());

//    setTimeout(() => {
//       setToday(Date.now());
//    }, 1000);
//    //https://devhints.io/wip/intl-datetime
//    const options ={
//       month: 'numeric',
//       day: 'numeric',
//       hour: 'numeric',
//       minute: 'numeric'
//    }
//    props.year_digits===4 ? options.year='numeric' : options.year='2-digit'
//    if (props.show_seconds===1) options.second = 'numeric';
//   //he-IL returns date separated with '.' instead of '/' like en-GB
//   const time = new Intl.DateTimeFormat('en-GB',options).format(today);
//   return <span>{time}</span>;
// }
