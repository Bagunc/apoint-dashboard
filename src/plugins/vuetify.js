import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  rtl: true,
  theme: {
    light: true,
    themes: {
      light: {
        info: '#E5F4FF',
        error: '#FFBDBD',
        accent: '#F49AC1',
        primary: '#3789C8',
        success: '#B0F759',
        warning: '#FFDA89',
        secondary: '#C39300',
      }
    }
  },
  icons: {
    iconfont: 'mdi',
  }
})
