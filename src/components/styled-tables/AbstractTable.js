import { tablePerPage } from '../../Utils'
import { isNumber } from 'lodash'

export default {
  name: "AbstractTable",
  props: {
    items: Array,
    config: Object,
    loading: Boolean,
  },
  data: function() {

    return {
      columns: this.config.headers
    }
  },
  computed: {
    /**
     * Checking is hidden footer (configured in config file)
     * 
     * @returns boolean
     */
    hideFooter() {

      return this.config.disablePagination
    },
    /**
     * Assembly of all settings and params for the table
     * 
     * @returns object
     */
    settings() {

      return {
        "items": this.items,
        "loading": this.loading,
        "totals": this.totalsRow,
        "headers": this.config.headers,
        "items-per-page": this.perPage,
        "hide-default-footer": this.hideFooter,
        "disable-sort": this.config.disableSort,
        "footer-props": this.config.footerProps,
        "disable-pagination": this.config.disablePagination,
      }
    },
    /**
     * Table perpage from localStorage if exists or default from config file
     * 
     * @returns number
     */
    perPage() {
      return tablePerPage(this.config.name) || this.config.itemsPerPage
    },
    /**
     * Registering vuetify element events
     * 
     * @returns object
     */
    events() {

      return {
        "update:items-per-page": this.updatePerPage
      }
    },
    /**
     * Totals row data calculating
     * 
     * @returns object
     */
    totalsRow() {
    
      if (!this.config.totals) return false
  
      const result = {}

      this.columns.forEach(col => {
        if (this.config.totalColumn === col.value) {
          result[col.value] = 'סיכום'
        } else if (col.ignor) {
          result[col.value] = '-'
        } else {
          result[col.value] = (
            this.items.reduce((sum, item) => {
              let value = parseFloat(item[col.value])
  
              if (!isNumber(value) || isNaN(value))
                value = 0
              
              return parseFloat(value) + sum
            }, 0)
          )
        }
      })

      return result
    },
  },
  methods: {
    /**
     * On table per page update event handler
     * 
     * @param { number } value
     * 
     * @returns undefined
     */
    updatePerPage: function(value) {
      
      tablePerPage(this.config.name, value)
    },
    /**
     * Table column highlighting
     * 
     * @param { string } key 
     * @param { string | object } column
     * 
     * @returns Object
     */
    highlightedColumn(key, column) {
      const result = {
        text: column,
        color: 'transparent',
      }

      const caption = this.headerCol(key)

      if (!caption) return result

      const { highlighting } = caption
      
      if (!highlighting) return result

      // min value in config or min value in column
      const min = highlighting.min.value || (
        Math.min.apply(null, this.items.map(function(item) {
          return item[key]
        }))
      )
      
      // max value in config or max value in column
      const max = highlighting.max.value || (
        Math.max.apply(null, this.items.map(function(item) {
          return item[key]
        }))
      )
      
      if (column <= min && highlighting.min.color)
        result.color = highlighting.min.color

      if (column && column >= max)
        result.color = highlighting.max.color

      return result
    },
    /**
     * Getting header column by key
     * 
     * @param { string } key 
     */
    headerCol(key) {

      return this.columns.filter(item => item.value === key)[0]
    }
  },
}
