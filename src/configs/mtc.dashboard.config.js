export const TableConfig = ({
  name: 'MTCTable',

  perPage: 26,
  totals: true,
  disableSort: false,
  disablePagination: false,
  totalColumn: 'fullAddress',
  caption: {
    color: '#fff',
    text: 'דוח מצב לבניינים',
    borderColor: '#FFB400',
    backgroundColor: 'rgba(55, 137, 200, 0.76)',
  },
  headers: [
    {
      text: 'בניין/ מתחם',
      value: 'fullAddress',
    },
    {
      text: 'אירועים פתוחים',
      value: 'totalOpen',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          value: 10,
          color: 'success',
        },
      },
    },
    {
      text: 'דחופים',
      value: 'totalUrgent',
    },
    {
      text: 'מעל SLA',
      value: 'totalOverDue',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'מעל שבוע',
      value: 'totalOverWeek',
      highlighting: {
        min: {
          color: 'success',
        },
        max: {
          color: 'error',
        },
      },
    },
    {
      text: 'מעל שבועיים',
      value: 'totalOver2Week',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'מעל חודש',
      value: 'totalOverMonth',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    }
  ],

  itemsPerPage: 26,
  footerProps: {
    itemsPerPageAllText: 'את כל',
    itemsPerPageText: 'שורות לעמוד:',
    itemsPerPageOptions: [26, 52, -1],
  },
})

export const Table1Config = ({
  name: 'MTCTable1',

  totals: true,
  disableSort: false,
  disablePagination: true,
  totalColumn: 'WorkerName',
  caption: {
    color: '#fff',
    borderColor: '#FFB400',
    text: 'דוח מצב אחראי תחזוקה',
    backgroundColor: 'rgba(55, 137, 200, 0.76)',
  },
  headers: [
    {
      text: 'אחראי',
      value: 'WorkerName',
    },
    {
      text: 'סהכ אירועים פתוחים',
      value: 'totalOpen',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'דחופים',
      value: 'totalUrgent',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'חדשים מהיום',
      value: 'newFromToday',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'נסגרו היום',
      value: 'closedToday',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      text: 'מעל SLA',
      value: 'totalOverDue',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    },
    {
      width: 130,
      value: 'totalOverDueUpdate',
      text: 'מעל SLA וללא עדכון שבוע',
      highlighting: {
        min: {
          color: 'error',
        },
        max: {
          color: 'success',
        },
      },
    }
  ],
})

export const Table2Config = ({
  name: 'MTCTable2',

  perPage: 12,
  disableSort: true,
  highlighting: false,
  disablePagination: false,
  caption: {
    color: '#fff',
    borderColor: '#F49AC1',
    text: '(אירועים אדומים (מעל שבוע וללא עדכון',
    backgroundColor: 'rgba(55, 137, 200, 0.76)',
  },
  headers: [
    {
      text: 'בניין/ מתחם',
      value: 'fullAddress',
      width: 150
    },
    {
      text: 'תאריך פתיחה',
      value: 'OpenDate',
    },
    {
      text: 'נושא',
      value: 'Subject',
    },
    {
      text: 'אחראי',
      value: 'WorkerName',
    },
    {
      text: 'עדכון אחרון',
      value: 'LastDateUpdate',
      width: 80
    },
  ],

  itemsPerPage: 10,
  footerProps: {
    itemsPerPageAllText: 'את כל',
    itemsPerPageText: 'שורות לעמוד:',
    itemsPerPageOptions: [10, 20, 40, 80, -1],
  },
})