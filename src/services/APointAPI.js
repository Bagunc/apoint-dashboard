const FETCH_DEFAULT_TIMEOUT = 10 * 1000; //10 seconds
const PROC_DEFAULT_TIMEOUT = 10 * 1000; //10 seconds
const FETCH_DEFAULT_HEADERS = {
  "Content-Type": "application/json"
};

let globalSessionToken;

export function apiParam(name, val, pType = 12 /* string */) {
  return { paramName: name, paramValue: val, paramType: pType }
}

export function apiGetSessionToken(CompID, userName, userPswd) {
  let raw = [
    apiParam("CompID", CompID, 16),
    apiParam("UserName", userName),
    apiParam("UserPswd", userPswd)
    // { paramName: "CompID", paramValue: CompID, paramType: 16 },
    // { paramName: "UserName", paramValue: userName, paramType: 12 },
    // { paramName: "UserPswd", paramValue: userPswd, paramType: 12 }
  ];
  const controller = new AbortController();
  const options = {
    method: "POST",
    headers: FETCH_DEFAULT_HEADERS,
    signal: controller.signal,
    body: JSON.stringify(raw),
    redirect: "follow"
  };
  let timeoutID = setTimeout(() => {
    controller.abort;
  }, FETCH_DEFAULT_TIMEOUT);

  // console.log(options);
  return fetch("https://api.apoint.co.il/dbActions/Login/Auth", options)
    .then(response => response.text()) //get text is async
    .then(result => {
      
      const response = JSON.parse(result)

      if (response.APIResponseCode === 0 && response.APIResponseDescription === 'Success')
        globalSessionToken = JSON.parse(response.APIResponseText).sessionToken;

      clearTimeout(timeoutID);

      return response;
    })
    .catch(error => {
      console.log(`apiGetSessionToken: ${error}`);
      throw error;
    });
}

export function callProc(procName, procParams, timeout = PROC_DEFAULT_TIMEOUT) {
  // console.log(`callProc: ${procName}`);
  const raw = [
    apiParam("sessionToken", globalSessionToken, 16),
    apiParam("procName", procName)
  ];
  if (procParams != undefined) {
    raw.push(procParams);
  }
  const controller = new AbortController();
  const options = {
    method: "POST",
    headers: FETCH_DEFAULT_HEADERS,
    signal: controller.signal,
    body: JSON.stringify(raw),
    redirect: "follow"
  };
  let timeoutID = setTimeout(() => {
    controller.abort;
  }, timeout);

  // console.log(options);
  return fetch("https://api.apoint.co.il/dbActions/callProc", options)
    .then(response => response.text()) //get text is async
    .then(result => {
      clearTimeout(timeoutID);
      //parse and clean extra api data
      //  console.log(`proc ${procName} result: ${result}`);
      let responseText = JSON.parse(result).APIResponseText;
      // console.log(`callProc APIResponseText: ${responseText}`);
      return responseText;
    })
    .catch(error => {
      console.log(`callProc: ${procName} ${error}`);
      throw error;
    });
}
