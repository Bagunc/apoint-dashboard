export default ({
  responsive: true,
  bezierCurve: false,
  lineWidth: 5,
  maintainAspectRatio: false,
  scales: {
    yAxes: [{
      display: true,
      position: 'right',
    }],
    xAxes:[{
      maxBarThickness: 100,
      ticks: {
        autoSkip: false,
        maxRotation: 45,
        minRotation: 45,
      },
      gridLines: {
        display: false,
        drawOnChartArea: true,
        offsetGridLines: true,
      }
    }]
  },
  elements: {
    line: {
      tension: 0,
      borderWidth: 3
    },
    point: {
      hitRadius: 0,
      rotation: 20
    }
  },
  legend: {
    display: false,
  }
})