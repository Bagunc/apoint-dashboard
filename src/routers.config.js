import Login from './routers/Login'
import MTCDashboard from './routers/MTCDashboard'
import MoneyDashboard from './routers/MoneyDashboard'

export const routers__map = {
  '/login': Login,
  '/login/': Login,

  '/dashboard': MTCDashboard,
  '/dashboard/': MTCDashboard,
  '/dashboard/mtc': MTCDashboard,
  
  '/dashboard/money': MoneyDashboard
}